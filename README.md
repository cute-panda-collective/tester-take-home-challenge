# Tester Take Home Challenge

Below is a link to a a Key Facts Sheet generator. Key Facts Sheets (KFS) are designed to allow you to easily compare home loan products offered by different lenders.

Click [here](https://www.onetwo.com.au/key-facts-sheet.html) to view the KFS generator.

Your challenge is to provide test coverage of the functionality which the Key Facts Sheet Generator provides.

## Requirements of your submission
Your tests can be in the form of: 
1. Manual scripts that, if needed, can be understood and run by anyone

OR

2. Automated test cases using any framework you like (ie: Jest or Cypress).

Your tests must also satisfy the functional requirements of the KFS generator (shown below).


## Functional requirements of the Key Facts Sheet generator

- Must not accept input above $2,000,000
- Generated KFS must:
    - Be printable
    - Displayed the inputted loan amount
    - Display an interest rate
    - Display a comparison rate
    - Display a monthly repayment amount

## Bonus points
Extra merit will be awared if you:
- Discover a genuine bug
- Your submission uses some form of automation
- Write test cases testing functionality beyond the requirements above

## How to submit
Please zip up a folder containing your challenge submission and email it to: career@onetwo.com.au
